import pandas as pd
from datetime import datetime


class FileProcessor:
    def __init__(self):
        self.filename_r = 'C:/Users/m.levinskaite/car_parts/Data_file.xlsx'
        self.filename_w = 'C:/Users/m.levinskaite/car_parts/data_file_processed.csv'

    def read_data(self):
        car_parts_data = pd.read_excel(self.filename_r)
        return car_parts_data

    def drop_empty_columns(self, car_parts_data):
        empty_cols = [col for col in car_parts_data.columns if car_parts_data[col].isnull().all()]
        car_parts_data.drop(empty_cols, axis=1, inplace=True)
        return car_parts_data

    def add_new_columns(self, car_parts_data):
        car_parts_data['year_created'] = car_parts_data['created_at'].dt.year
        car_parts_data['month_created'] = car_parts_data['created_at'].dt.month
        car_parts_data['day_created'] = car_parts_data['created_at'].dt.day
        car_parts_data['week_created'] = car_parts_data['created_at'].dt.isocalendar().week
        car_parts_data['weekday_created'] = car_parts_data['created_at'].dt.weekday

        car_parts_data['year_deleted'] = car_parts_data['deleted_at'].dt.year
        car_parts_data['month_deleted'] = car_parts_data['deleted_at'].dt.month
        car_parts_data['day_deleted'] = car_parts_data['deleted_at'].dt.day
        car_parts_data['week_deleted'] = car_parts_data['deleted_at'].dt.isocalendar().week
        car_parts_data['weekday_deleted'] = car_parts_data['deleted_at'].dt.weekday

        car_parts_data['year_scrap_added'] = car_parts_data['scrapheap_date_added'].dt.year
        car_parts_data['month_scrap_added'] = car_parts_data['scrapheap_date_added'].dt.month
        car_parts_data['day_scrap_added'] = car_parts_data['scrapheap_date_added'].dt.day
        car_parts_data['week_scrap_added'] = car_parts_data['scrapheap_date_added'].dt.isocalendar().week
        car_parts_data['weekday_scrap_added'] = car_parts_data['scrapheap_date_added'].dt.weekday

        car_parts_data['year_sold'] = car_parts_data['sold_at'].dt.year
        car_parts_data['month_sold'] = car_parts_data['sold_at'].dt.month
        car_parts_data['day_sold'] = car_parts_data['sold_at'].dt.day
        car_parts_data['week_sold'] = car_parts_data['sold_at'].dt.isocalendar().week
        car_parts_data['weekday_sold'] = car_parts_data['sold_at'].dt.weekday

        car_parts_data['margin'] = car_parts_data['part_price'] - car_parts_data['order_part_price']

        car_parts_data['potential_order_price'] = car_parts_data['part_price'] * 0.8264
        car_parts_data['potential_margin'] = car_parts_data['part_price'] - car_parts_data[
            'potential_order_price']

        car_parts_data['sold_in_days'] = (car_parts_data['sold_at'] - car_parts_data['created_at']).dt.days
        today = datetime.today()
        car_parts_data['days_on_market'] = (today - car_parts_data['created_at']).dt.days

        return car_parts_data

    def map_status_name(self, car_parts_data):
        if car_parts_data['status'] == 0:
            return 'Canceled'
        if car_parts_data['status'] == 1:
            return 'Not ready'
        if car_parts_data['status'] == 2:
            return 'Waiting for courier'
        if car_parts_data['status'] == 3:
            return 'Delivered to courier'
        if car_parts_data['status'] == 4:
            return 'On the way'
        if car_parts_data['status'] == 5:
            return 'Delivered'
        if car_parts_data['status'] == 6:
            return 'Awaiting refund'
        if car_parts_data['status'] == 7:
            return 'Refunded'
        if car_parts_data['status'] == 8:
            return 'Courier returned'
        else:
            return 'No shipment data'

    def save_data_to_csv(self, car_parts_data):
        car_parts_data.to_csv(self.filename_w)
        return


file_processor = FileProcessor()
car_parts = file_processor.read_data()
car_parts = file_processor.drop_empty_columns(car_parts)
car_parts['status_name'] = car_parts.apply(file_processor.map_status_name, axis=1)
file_processor.save_data_to_csv(car_parts)
